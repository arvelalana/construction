<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Aug 10, 2017
     */
    class Controller_Construction extends WController {
        
        protected $org_id = null;
        
        public function __construct() {
            parent::__construct();
            
            $wapp = WApp::instance();
            $web_code = $wapp->get_web_code();
            $org_code = $wapp->get_org_code();

            $q = "SELECT * FROM org WHERE web_code = " . db::escape($web_code) . " AND org_code = " . db::escape($org_code) . " AND status > 0";
            $r = db::get_row($q);
            if (isset($r)) {
                $this->org_id = $r['org_id'];
            }
            
            $user = $wapp->get_user();
            if (isset($user)) {
                $this->org_id = element('org_id', $user);
            }
         
        }
    }
    