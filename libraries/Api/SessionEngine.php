<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Oct 17, 2016
     */
    class Api_SessionEngine extends Api_Engine {

        public function __construct() {
            
        }

        public static function instance() {
            if (self::$instance == null) {
                self::$instance = new Api_SessionEngine();
            }
            return self::$instance;
        }

        public function exec($post) {
            $ci = & get_instance();

            $err_code = 0;
            $err_message = '';

            $auth_id = element('auth_id', $post);
            if ($this->error()->get_err_code() == "0") {
                if (strlen($auth_id) == 0) {
                    $this->error()->add("AUTH02", lang("Auth ID is required"));
                }
            }

            if ($this->error()->get_err_code() == 0) {
                $request_ip_address = $ci->input->ip_address();

                $q = "SELECT * 
                FROM api_auth 
                WHERE md5(CONCAT(org_id, username, password)) = " . $ci->db->escape($auth_id) . "
                    AND status > 0
                    ";
                $r = Helpers_DB::get_row($q);
          
                if (isset($r)) {
                    $ip_address = element('ip_address', $r);
                    $org_id = element('org_id', $r, 0);

                    $allow = false;
                    if ($ip_address == '*') {
                        $allow = true;
                    }
                    else {
                        $list_ip = json_decode($ip_address, true);
                        if (in_array($request_ip_address, $list_ip)) {
                            $allow = true;
                        }
                        else {
                            // ip is not registered
                        }
                    }
                    if ($allow) {
                        $q = "SELECT * FROM org WHERE org_id = " .db::escape($org_id);
                        $r = db::get_row($q);
                        
                        if (isset($r)) {
                            $session = Api_SessionConstruction::instance(null, $org_id);
                            $org_type = element('org_type', $r);
                            $this->data = array(
                                'session_id' => $session->get_session_id(),
//                                'org_type' => $org_type,
                                'org_id' => $org_id,
                            );
                            $session->set('org', $r);
                            $session->set('org_id', $org_id);
                            $session->set('url_front', $r['url_front']);
                            $session->set('ip_address', $request_ip_address);
                            $session->set('wapp_logged_in', 1);
                            $this->response['session_id'] = $session->get_session_id();
                        }
                        else {
                            $this->error()->add("AUTH01", lang("Username / password tidak valid"));
                        }
                    }
                    else {
                        $this->error()->add("AUTH02", lang("IP Address is not registered"));
                    }
                }
                else {
                    $this->error()->add("AUTH01", lang("Username / password tidak valid"));
                }
            }
            return false;
        }

    }
    