<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Import_ProductCategory extends Api_Import {

        protected $import_engine;

        public function __construct(Api_ImportEngine $import_engine) {
            parent::__construct($import_engine);
            $this->import_engine = $import_engine;
        }

        public function exec($post) {
            $wapp = WApp::instance();
            $ci = & get_instance();

            $org_id = $this->session->get('org_id');
            $files = $_FILES;
            $name = 'filename';
            $file = $files[$name];
            $document_name = $file['name'];
            $document_tmp_path = $file['tmp_name'];

            $err_code = 0;
            $err_message = '';
            $data = array();

            try {
                $resource = Resource::factory('document', $name);

                if ($err_code == 0) {
                    $filename = $resource->save($document_name, $file);
                    $secure_filename = $resource->get_filename(true);
                    $path_filename = $resource->get_path_filename();
                    $original_filename = $resource->get_original_filename();
                    $url_preview = base_url() . 'resource/document/' . urlencode($secure_filename);
                    $data = array(
                        'filename' => $secure_filename,
                        'uri_filename' => $original_filename,
                        'ori_filename' => $document_name,
                        'url_preview' => $url_preview,
                        'url' => base_url() . 'resource/document/' . urlencode($secure_filename),
                    );
                }
            }
            catch (Exception $exc) {
                $err_code++;
                $err_message = $exc->getMessage();
            }

            $last_sync_date = null;
            $data_sync = array(
                'last_sync_date' => date("Y-m-d H:i:s")
            );

            $q = "SELECT * FROM sync WHERE module = 'product_category' AND status > 0";
            $r = db::get_row($q);
            if (isset($r)) {
                $last_sync_date = $r['last_sync_date'];
                $ci->db->update('sync', $data_sync, array('module' => 'product_category'));
            }
            else {
                $data_sync['module'] = 'product_category';
                $data_sync['created'] = date("Y-m-d H:i:s");
                $data_sync['updated'] = date("Y-m-d H:i:s");
                $ci->db->insert('sync', $data_sync);
            } 


            $total_insert = 0;
            $total_update = 0;
            $total_no_update = 0;
            $total_data = 0;
            $file_path = $path_filename;
//            $file_path = FCPATH . 'upload/construction/' . 'Format Kategori Barang.csv';
            $row = 0;
            if (($handle = fopen($file_path, "r")) !== FALSE) {
                while (($arr_data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    if ($row > 0) {
                        
                        $product_category_code = element(0, $arr_data);
                        $product_category_name = element(1, $arr_data);
                        $void = element(2, $arr_data);
                        $last_time = element(3, $arr_data);
                        $last_time = date("Y-m-d H:i:s", strtotime($last_time));

                        if (strtotime($last_time) >= strtotime($last_sync_date)) {
                            $status = '1';
                            if ($void == 1) {
                                $status = 0;
                            }

                            $data_product_category = array(
                                'org_id' => $org_id,
                                'product_category_code' => $product_category_code,
                                'product_category_name' => $product_category_name,
                                'last_update_time' => $last_time,
                                'updated' => $last_time,
                                'status' => $status,
                            );
                            $q = "SELECT * 
                            FROM product_category 
                            WHERE product_category_code = " . db::escape($product_category_code) . " ";
                            $r = db::get_row($q);
                            if (isset($r)) {
                                $total_update++;
                                $ci->db->update('product_category', $data_product_category, array('product_category_code' => $product_category_code));
                            }
                            else {
                                $total_insert++;
                                $ci->db->insert('product_category', $data_product_category);
                            }
                        }
                        else {
                            $total_no_update++;
                        }
                        $total_data++;
                    }
                    $row++;
                }
                fclose($handle);
            }

            $status = 'SUCCESS';
            if ($err_code > 0) {
                $status = 'ERROR';
            }
            $data['total_data'] = $total_data;
            $data['total_insert'] = $total_insert;
            $data['total_update'] = $total_update;
            $data['total_no_update'] = $total_no_update;

            $return = array(
                'status' => $status,
                'message' => $err_message,
                'data' => $data,
            );
            return $return;
        }

    }
    