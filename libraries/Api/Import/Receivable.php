<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Import_Receivable extends Api_Import {

        protected $import_engine;

        public function __construct(Api_ImportEngine $import_engine) {
            parent::__construct($import_engine);
            $this->import_engine = $import_engine;
        }

        public function exec($post) {
            $wapp = WApp::instance();
            $ci = & get_instance();

            $org_id = $this->session->get('org_id');
            $name = 'filename';
            $files = $_FILES;
            $file = $files[$name];
            $document_name = $file['name'];
            $document_tmp_path = $file['tmp_name'];

            $err_code = 0;
            $err_message = '';
            $data = array();

            try {
                $resource = Resource::factory('document', $name);

                if ($err_code == 0) {
                    $filename = $resource->save($document_name, $file);
                    $secure_filename = $resource->get_filename(true);
                    $path_filename = $resource->get_path_filename();
                    $original_filename = $resource->get_original_filename();
                    $url_preview = base_url() . 'resource/document/' . urlencode($secure_filename);
                    $data = array(
                        'filename' => $secure_filename,
                        'uri_filename' => $original_filename,
                        'ori_filename' => $document_name,
                        'url_preview' => $url_preview,
                        'url' => base_url() . 'resource/document/' . urlencode($secure_filename),
                    );
                }
            }
            catch (Exception $exc) {
                $err_code++;
                $err_message = $exc->getMessage();
            }

            $last_sync_date = null;
            $data_sync = array(
                'last_sync_date' => date("Y-m-d H:i:s")
            );

            $q = "SELECT * FROM sync WHERE module = 'receivable' AND status > 0";
            $r = db::get_row($q);
            if (isset($r)) {
                $last_sync_date = $r['last_sync_date'];
                $ci->db->update('sync', $data_sync, array('module' => 'receivable'));
            }
            else {
                $data_sync['module'] = 'receivable';
                $data_sync['created'] = date("Y-m-d H:i:s");
                $data_sync['updated'] = date("Y-m-d H:i:s");
                $ci->db->insert('sync', $data_sync);
            }


            $total_insert = 0;
            $total_update = 0;
            $total_no_update = 0;
            $total_data = 0;
//            $file_path = FCPATH . 'upload/construction/receivable/' . 'Format Piutang.csv';
            $file_path = $path_filename;
            $row = 0;
            if (($handle = fopen($file_path, "r")) !== FALSE) {
                $ci->db->delete('receivable', array('status' => 1));
                while (($arr_data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    if ($row > 0) {
                        $ref_id_import = element(0, $arr_data);
                        $coa_code = element(1, $arr_data);
                        $receivable_date = element(2, $arr_data);
                        $invoice_code = element(3, $arr_data);
                        $due_date = element(4, $arr_data);
                        $description = element(5, $arr_data);
                        $total_amount = element(6, $arr_data);
//                        $paid_off_ref_code = element(7, $arr_data);
//                        $paid_off_status = element(8, $arr_data);
//                        $paid_off_date = element(9, $arr_data);


                        $receivable_date = date("Y-m-d H:i:s", strtotime($receivable_date));
                        $due_date = date("Y-m-d H:i:s", strtotime($due_date));
//                        $paid_off_date = date("Y-m-d H:i:s", strtotime($paid_off_date));

                        $data_receivable = array(
                            'org_id' => $org_id,
                            'coa_code' => $coa_code,
                            'receivable_date' => $receivable_date,
                            'receivable_due_date' => $due_date,
                            'amount' => $total_amount,
                            'ref_code' => $invoice_code,
                            'description' => $description,
//                            'paid_off_code' => $paid_off_ref_code,
//                            'paid_off_date' => $paid_off_date,
//                            'paid_off_status' => $paid_off_status,
                            'ref_id_import' => $ref_id_import,
                            'updated' => date("Y-m-d H:i:s"),
                            'status' => 1,
                        );
//                        $q = "SELECT * 
//                        FROM receivable 
//                        WHERE ref_id_import = " . db::escape($ref_id_import) . " ";
//                        $r = db::get_row($q);
//                        if (isset($r)) {
//                            $total_update++;
//                            $ci->db->update('receivable', $data_receivable, array('ref_id_import' => $ref_id_import));
//                        }
//                        else {
                        $total_insert++;
                        $ci->db->insert('receivable', $data_receivable);
//                        }

                        $total_data++;
                    }
                    $row++;
                }
                fclose($handle);
            }

            $status = 'SUCCESS';
            if ($err_code > 0) {
                $status = 'ERROR';
            }
            $data['total_data'] = $total_data;
            $data['total_insert'] = $total_insert;
            $data['total_update'] = $total_update;
            $data['total_no_update'] = $total_no_update;

            $return = array(
                'status' => $status,
                'message' => $err_message,
                'data' => $data,
            );
            return $return;
        }

    }
    