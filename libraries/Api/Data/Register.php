<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Data_Register extends Api_Data {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');
            $org_id = $this->session->get('org_id');
            $err_code = 0;
            $err_message = '';

            $fullname = element('name', $post);
            $phone1 = element('phone', $post);
            $password = element('password', $post);
            $confirm_password = element('confirm_password', $post);

            if ($err_code == 0) {
                if (strlen($fullname) == 0) {
                    $err_code++;
                    $err_message = lang('Nama harus diisi');
                }
            }

            if ($err_code == 0) {
                if (strlen($phone1) == 0) {
                    $err_code++;
                    $err_message = lang('No Handphone harus diisi');
                }
            }

            if ($err_code == 0) {
                if (strlen($password) == 0) {
                    $err_code++;
                    $err_message = lang('Kata Sandi harus diisi');
                }
            }

            if ($err_code == 0) {
                if (strlen($confirm_password) == 0) {
                    $err_code++;
                    $err_message = lang('Konfirmasi Kata Sandi harus diisi');
                }
            }

            if ($err_code == 0) {
                if ($password != $confirm_password) {
                    $err_code++;
                    $err_message = lang('Kata Sandi dan Konfirmasi Kata Sandi harus sama');
                }
            }

            if ($err_code == 0) {
                $q = "SELECT COUNT(*) 
                    FROM users 
                    WHERE username = " . db::escape($phone1) . " AND status > 0";
                $r = db::get_value($q);
                if ($r > 0) {
                    $err_code++;
                    $err_message = lang('No Handphone sudah terdaftar');
                }
            }

            if ($err_code == 0) {
                $data_member = array(
                    'first_name' => $fullname,
                    'phone1' => $phone1,
                    'created' => date("Y-m-d H:i:s"),
                    'createdby' => $username,
                    'updated' => date("Y-m-d H:i:s"),
                    'updatedby' => $username,
                    'status' => 1,
                );
                $ci->db->insert('member', $data_member);
                $member_id = $ci->db->insert_id();

                $data_user = array(
                    'org_id' => $org_id,
                    'member_id' => $member_id,
                    'first_name' => $fullname,
                    'username' => $phone1,
                    'password' => md5($password),
                    'created' => date("Y-m-d H:i:s"),
                    'createdby' => $username,
                    'updated' => date("Y-m-d H:i:s"),
                    'updatedby' => $username,
                    'status' => 1,
                );
                $ci->db->insert('users', $data_user);
            }

            if ($err_code > 0) {
                $this->error()->add("REGISTER[01]", $err_message);
            }

            return $return;
        }

    }
    