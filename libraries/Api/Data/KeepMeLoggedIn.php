<?php

    /**
     *
     * @author Arvel Alana
     * @since  Feb 20, 2017
     */
    class Api_Data_KeepMeLoggedIn extends Api_Data {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $err_code = 0;
            $err_message = '';
            $username = '';
            $user_token = '';
            $token = '';

            $username = element('username', $post);
            $user_token = element('user_token', $post);
            $token = element('token', $post);


            if ($err_code == 0) {
                if (strlen($username) == 0) {
                    $err_code++;
                    $err_message = lang('Username is required');
                }
            }

            if ($err_code == 0) {
                if (strlen($user_token) == 0) {
                    $err_code++;
                    $err_message = lang('User token is required');
                }
            }
            if ($err_code == 0) {
                if (strlen($token) == 0) {
                    $err_code++;
                    $err_message = lang('Token is required');
                }
            }


            if ($err_code == 0) {

                $q = "SELECT u.*,nt.*,m.coa_code
                    FROM users u
                    INNER JOIN notif_token nt ON u.user_id = nt.user_id
                    LEFT JOIN member m ON u.member_id = m.member_id
                    WHERE u.username = " . $ci->db->escape($username) . " AND
                        nt.user_token = " . $ci->db->escape($user_token) . " AND 
                        nt.status_token = 'ACTIVE' AND
                        u.org_id = " . $this->session->get('org_id') . " AND
                        nt.token = " . $ci->db->escape($token) . " AND 
                        u.status > 0
                    ";

                $r = db::get_row($q);
                if (isset($r)) {
                    $return = $r;
                    // login success set session
                    $this->session->set('user_login', $r);
                    $this->session->set('user_id', $r['user_id']);
                    $this->session->set('member_id', $r['member_id']);
                    $this->session->set('role_id', $r['role_id']);
                    $this->session->set('username', $r['username']);
                    $this->session->set('user_logged_in', true);
                    $this->session->set('user_token', $r['user_token']);
                    $notif_token = new Api_Data_NotifToken($this->data_engine);
                    $return['notif'] = $notif_token->exec($post);
                    
                    $return += $this->get_role_permission();
                }
                else {
                    $err_code++;
                    $err_message = lang('Username / Token is wrong');
                }
            }

            if ($err_code > 0) {
                $this->error()->add("LOGIN[01]", $err_message);
            }

            return $return;
        }

    }
    