<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Feb 21, 2017
     */
    class Api_Data_NotifToken extends Api_Data_UserLogged {

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();
            $org_id = $this->session->get('org_id');
            $username = '';
            $user_token = '';
            $err_code = 0;
            $err_message = '';

            $token = element('token', $post); 
            $device_name = element('device_name', $post); 
            $connection = element('connection', $post); 
            $ip_address = element('ip_address', $post); 
            
            $user_id = $this->session->get('user_id');
            $username = $this->session->get('username');
            $user_token = $this->session->get('user_token');


            if ($err_code == 0) {
                if (strlen($token) == 0) {
                    $err_code++;
                    $err_message = lang('Token is required');
                }
            }

            if ($err_code == 0) {
                if ($this->session->get('user_logged_in')) {
                    if (strlen($username) == 0) {
                        $err_code++;
                        $err_message = lang('Username is required');
                    }
                }
            }

            if ($err_code == 0) {
                $data_token = array(
                    'org_id' => $org_id,
                    'user_id' => $user_id,
                    'username' => $username,
                    'token' => $token,
                    'user_token' => $user_token,
                    'device_name' => $device_name,
                    'connection' => $connection,
                    'ip_address' => $ip_address,
                    'status_token' => 'ACTIVE',
                    'created' => date("Y-m-d H:i:s"),
                    'createdby' => $username,
                    'updated' => date("Y-m-d H:i:s"),
                    'updatedby' => $username,
                    'status' => 1,
                );

                $q = "SELECT count(notif_token_id) 
                    FROM notif_token 
                    WHERE token = " . $ci->db->escape($token) . "
                        AND org_id = " . $ci->db->escape($org_id) . " AND status > 0";
                $r = Helpers_DB::get_value($q);
                if ($r > 0) {
                    // token found, update it
                    $ci->db->update('notif_token', $data_token, array('token' => $token));
                }
                else { 
                    // token not found, then insert new row
                    $ci->db->insert('notif_token', $data_token);
                }
            }

            $total_notif = 0;
            if ($err_code == 0) {
                $additional_where = '';
//                if ($this->session->get('user_logged_in')) {
//                    $additional_where.="AND nt.username = " . $ci->db->escape($this->session->get('username'));
//                }
//                else {
                    $additional_where.=" AND nt.token = " . $ci->db->escape($token);
//                }

                $q = "
                    SELECT count(*)
                    FROM 
                        (SELECT nm.notif_message_id
                        FROM notif_message nm
                        INNER JOIN notif_message_detail nmd ON nm.notif_message_id = nmd.notif_message_id
                        INNER JOIN notif_token nt ON nt.notif_token_id = nmd.notif_token_id
                        INNER JOIN org o ON o.web_code = nm.web_code AND o.app_code = nm.app_code AND o.org_code = nm.org_code
                        WHERE o.org_id = " . $ci->db->escape($org_id) . "
                            " . $additional_where . " 
                            AND nmd.status_message = 'SUCCESS' 
                            AND nm.reference_table = 'notification'
                            AND nmd.read = 0 AND nm.status > 0
                        GROUP BY nm.notif_message_id
                        ORDER BY nmd.created DESC) as a";
                $total_notif = Helpers_DB::get_value($q);
            }
            
            if ($err_code > 0) {
                $this->error()->add("NTFTKN[01]", $err_message);
            }

            if ($err_code == 0) {
                $return = array(
                    'message' => 'success',
                    'total_notif' => $total_notif
                );
            }

            return $return;
        }

    }
    