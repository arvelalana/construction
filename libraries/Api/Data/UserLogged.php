<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Nov 14, 2016
     */
    class Api_Data_UserLogged extends Api_Data {
        
        protected $username;
        protected $user;
        protected $user_id;
        protected $role_id;
        protected $logged_in = true;
        protected $data_engine;
        
        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            
            $this->data_engine = $data_engine;
            
            if (strlen($this->session->get('user_logged_in')) == 0) {
                $this->logged_in = false;
                $this->error()->add("Login[01]", lang("User belum login"));
            }
            
            if ($this->error()->get_err_code() == 0) {
                $this->user = $this->session->get('user_login');
                $this->user_id = element('user_id', $this->user);
                $this->role_id = element('role_id', $this->user);
                $this->username = element('username', $this->user);
            }
        }
        
        
    }
    