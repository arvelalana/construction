<?php

    /**
     *
     * @author Arvel Alana
     * @since  Feb 20, 2017
     */
    class Api_Data_NotifReceived extends Api_Data {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');
            
            $err_code = 0;
            $err_message = '';
            $notification_id = '';
            $token = '';

            $notification_id = element('notification_id', $post);
            $token = element('token', $post);


            if ($err_code == 0) {
                if (strlen($notification_id) == 0) {
                    $err_code++;
                    $err_message = lang('Notif id is required');
                }
            }

            if ($err_code == 0) {
                if (strlen($token) == 0) {
                    $err_code++;
                    $err_message = lang('Token is required');
                }
            }


            if ($err_code == 0) {

                $q = "SELECT nd.notif_message_detail_id,n.message as content
                        FROM notification n
                        INNER JOIN org o ON n.org_id = o.org_id
                        INNER JOIN notif_message nm ON nm.reference_id =  n.notification_id
                        INNER JOIN notif_message_detail nd ON nd.notif_message_id =  nm.notif_message_id 
                        INNER JOIN notif_token nt ON nt.notif_token_id = nd.notif_token_id 
                        AND nt.token = " . db::escape($token) . "
                        WHERE n.notification_id = " . db::escape($notification_id) . "
                        AND n.status > 0 and nt.status > 0";

                $r = db::get_row($q);
                if (isset($r)) {
                    $data_notif = array(
                        'is_received' => 1,
                        'received_message' => $r['content'],
                        'updated' => date("Y-m-d H:i:s"),
                        'updatedby' => $username
                    );
                    $ci->db->update('notif_message_detail', $data_notif, array('notif_message_detail_id' => $r['notif_message_detail_id']));
                }
                else {
                    $err_code++;
                    $err_message = lang('Not Found');
                }
            }
            if ($err_code == 0) {
                $return['message'] = lang('Status Notif Message berhasil diubah');
            }

            if ($err_code > 0) {
                $this->error()->add("NR[01]", $err_message);
            }

            return $return;
        }

    }
    