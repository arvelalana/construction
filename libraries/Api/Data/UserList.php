<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Data_UserList extends Api_Data_UserLogged {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }
        
        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');
            
            $err_code = 0;
            $err_message = '';
            $data = array();
            
            if ($err_code == 0) {
                $q = "SELECT u.*, m.email1, m.phone1, m.coa_code
                    FROM users u
                    LEFT JOIN member m ON m.member_id = u.member_id
                    WHERE u.status > 0
                    ";
                $r = db::get_result($q);
                foreach ($r as $k => $v) {
                    $data[] = array(
                        'user_id' => $v['user_id'],
                        'member_id' => $v['member_id'],
                        'username' => $v['username'],
                        'first_name' => $v['first_name'],
                        'email1' => $v['email1'],
                        'phone1' => $v['phone1'],
                        'coa_code' => $v['coa_code'],
                    );
                }
            }
            $return['results'] = $data;
            
            if ($err_code > 0) {
                $this->error()->add("USERLIST[01]", $err_message);
            }

            return $return;
        }
    }
    