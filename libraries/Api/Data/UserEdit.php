<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Data_UserEdit extends Api_Data_UserLogged {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');

            $err_code = 0;
            $err_message = '';

            $user_id = element('user_id', $post);
            $is_member = element('is_member', $post);
            $fullname = element('name', $post);
            $phone1 = element('phone1', $post);
            $coa_code = element('coa_code', $post);
            $password = element('password', $post);
            $confirm_password = element('confirm_password', $post);

            $is_add = true;
            if (strlen($user_id) > 0) {
                $is_add = false;
            }

            if ($err_code == 0) {
                if (strlen($fullname) == 0) {
                    $err_code++;
                    $err_message = lang('Nama harus diisi');
                }
            }

            if ($err_code == 0) {
                if (strlen($phone1) == 0) {
                    $err_code++;
                    $err_message = lang('No Handphone harus diisi');
                }
            }
            
            if ($err_code == 0) {
                if ($is_member && strlen($coa_code) == 0) {
                    $err_code++;
                    $err_message = lang('Kode COA harus diisi');
                }
            }

            if ($is_add) {
                if ($err_code == 0) {
                    if (strlen($password) == 0) {
                        $err_code++;
                        $err_message = lang('Kata Sandi harus diisi');
                    }
                }

                if (strlen($password) > 0) {
                    if ($err_code == 0) {
                        if (strlen($confirm_password) == 0) {
                            $err_code++;
                            $err_message = lang('Konfirmasi Kata Sandi harus diisi');
                        }
                    }

                    if ($err_code == 0) {
                        if ($password != $confirm_password) {
                            $err_code++;
                            $err_message = lang('Kata Sandi dan Konfirmasi Kata Sandi harus diisi');
                        }
                    }
                }
            }

            $member_id = null;
            if ($err_code == 0) {
                $where = '';
                if (!$is_add) {
                    $where .= ' AND user_id <> ' . db::escape($user_id);
                }
                $q = "SELECT *
                    FROM users 
                    WHERE username = " . db::escape($phone1) . " " . $where . " AND status > 0";
                $r = db::get_row($q);
                if (isset($r)) {
                    $err_code++;
                    $err_message = lang('No Handphone sudah terdaftar');
                }
            }

            if ($err_code == 0) {
                $data_member = array(
                    'first_name' => $fullname,
                    'phone1' => $phone1,
                    'coa_code' => $coa_code,
                    'updated' => date("Y-m-d H:i:s"),
                    'updatedby' => $username,
                    'status' => 1,
                );
                if ($is_member) {
                    if ($is_add) {
                        $data_member['created'] = date("Y-m-d H:i:s");
                        $data_member['createdby'] = $username;
                        $ci->db->insert('member', $data_member);
                        $member_id = $ci->db->insert_id();
                    }
                    else {
                        $q = "
                            SELECT member_id
                            FROM users 
                            WHERE user_id = " . db::escape($user_id) ." AND status > 0";
                        $member_id = db::get_value($q);
                        
                        if ($member_id != null) {
                            $ci->db->update('member', $data_member, array('member_id' => $member_id));
                        }
                    }
                }
//                echo $ci->db->last_query() .'<br/>';

                $data_user = array(
                    'member_id' => $member_id,
                    'first_name' => $fullname,
                    'username' => $phone1,
                    'updated' => date("Y-m-d H:i:s"),
                    'updatedby' => $username,
                    'status' => 1,
                );
                if ($is_add) {
                    $data_user['password'] = md5($password);
                    $data_user['created'] = date("Y-m-d H:i:s");
                    $data_user['createdby'] = $username;
                    $ci->db->insert('users', $data_user);
                }
                else {
                    if (strlen($password) > 0) {
                        $data_user['password'] = md5($password);
                    }
                    $ci->db->update('users', $data_user, array('user_id' => $user_id));
                }
//                echo $ci->db->last_query() .'<br/>';
            }

            if ($err_code == 0) {
                $return['err_code'] = $err_code;
                if ($is_add) {
                    $return['message'] = lang('Member berhasil ditambahkan');
                }
                else {
                    $return['message'] = lang('Member berhasil diubah');
                }
            }

            if ($err_code > 0) {
                $this->error()->add("USREDT[01]", $err_message);
            }

            return $return;
        }

    }
    