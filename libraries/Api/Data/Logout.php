<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Feb 20, 2017
     */
    class Api_Data_Logout extends Api_Data_UserLogged {

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $err_code = 0;
            $err_message = '';
            $token = '';
            $org_id = $this->session->get('org_id');
            $token = element('token', $post);

            if ($err_code == 0) {
                if (strlen($token) == 0) {
                    $err_code++;
                    $err_message = lang('Token is required');
                }
            }

            if ($err_code == 0) {
                $user = $this->session->get('user_login');
                $username = element('username', $user);

                $notif_token = new Api_Data_NotifToken($this->data_engine);
                $return['notif'] = $notif_token->exec($post);
                
                // login success set session
                $this->session->set('username', null);
                $this->session->set('user_login', array());
                $this->session->set('user_logged_in', false);

            }

            if ($err_code == 0) {
                $ci->db->update('notif_token', array('status_token' => 'NOT_ACTIVE'), array('token' => $token));
            }
            if ($err_code > 0) {
                $this->error()->add("LOGOUT[01]", $err_message);
            }

            return $return;
        }

    }
    