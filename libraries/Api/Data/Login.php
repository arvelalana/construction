<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Feb 20, 2017
     */
    class Api_Data_Login extends Api_Data {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $err_code = 0;
            $err_message = '';
            $user_token = '';

            $username = element('username', $post);
            $password = element('password', $post);

            if ($err_code == 0) {
                if (strlen($username) == 0) {
                    $err_code++;
                    $err_message = lang('Username is required');
                }
            }

            if ($err_code == 0) {
                if (strlen($password) == 0) {
                    $err_code++;
                    $err_message = lang('Password is required');
                }
            }


            if ($err_code == 0) {

                $update = "UPDATE users 
                           SET user_token = md5(CONCAT(current_date,current_time,username,password)) 
                           WHERE username = " . db::escape($username);
                $ci->db->query($update);

                $q = "SELECT u.*,m.coa_code
                    FROM users u
                    LEFT JOIN member m ON u.member_id = m.member_id
                    WHERE u.username = " . $ci->db->escape($username) . " AND
                        u.password = " . $ci->db->escape(md5($password)) . " AND 
                        u.org_id = " . $this->session->get('org_id') . " AND
                        u.status > 0
                    ";

                $r = db::get_row($q);
                if (isset($r)) {
                    $return = $r;
                    // login success set session
                    $this->session->set('user_login', $r);
                    $this->session->set('user_token', $r['user_token']);
                    $this->session->set('user_id', $r['user_id']);
                    $this->session->set('member_id', $r['member_id']);
                    $this->session->set('role_id', $r['role_id']);
                    $this->session->set('username', $r['username']);
                    $this->session->set('user_logged_in', true);
                    $notif_token = new Api_Data_NotifToken($this->data_engine);
                    $return['notif'] = $notif_token->exec($post);
                    
                    $return += $this->get_role_permission();
                }
                else {
                    $err_code++;
                    $err_message = lang('Username / Password is wrong');
                }
            }

            if ($err_code > 0) {
                $this->error()->add("LOGIN[01]", $err_message);
            }

            return $return;
        }

    }
    