<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Data_ProductCategoryList extends Api_Data_UserLogged {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');

            $err_code = 0;
            $err_message = '';
            $data = array();

            if ($err_code == 0) {
                $q = "SELECT pc.*
                    FROM product_category pc
                    WHERE pc.status > 0";
                $r = db::get_result($q);
                $data = $r;
                $return['list'] = $data;
            }

            if ($err_code > 0) {
                $this->error()->add("PRODUCT_CATEGORY[01]", $err_message);
            }

            return $return;
        }

    }
    