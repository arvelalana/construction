<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Data_NotificationList extends Api_Data_UserLogged {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');
            $member_id = $this->session->get('member_id');
            $org_id = $this->session->get('org_id');
            $token = element('token', $post);

            $err_code = 0;
            $err_message = '';
            $data = array();

            if ($err_code == 0) {
                $where = '';

                $arr_where = array();
                if (strlen($member_id) > 0) {
                    $arr_where[] = " n.org_id = " . db::escape($org_id) . " ";
                }

//                if ($this->session->get('user_logged_in')) {
//                    $arr_where[] = " nt.username = " . $ci->db->escape($this->session->get('username'));
//                }
//                else {
                $arr_where[] = " nt.token = " . $ci->db->escape($token);
//                }


                if (count($arr_where) > 0) {
                    $where = implode(' AND ', $arr_where) . ' AND ';
                }


                $q = "SELECT n.*
                    FROM notification n
                    WHERE n.status > 0
                    ORDER BY created DESC";
                $r = db::get_result($q);
                foreach ($r as $key => $val) {
                    $read = element('read', $val);
                    $notif_message_id = element('notif_message_id', $val);
                    $notif_token_id = element('notif_token_id', $val);

                    $data[] = array(
                        'notification_id' => $val['notification_id'],
                        'org_id' => $val['org_id'],
                        'member_id' => $val['member_id'],
                        'message' => $val['message'],
                        'read' => $read,
                        'created' => date('d F Y', strtotime($val['created'])),
                        'created_time' => date('H:i', strtotime($val['created'])),
                        'updated' => date('d F Y', strtotime($val['updated']))
                    );
                }
                $q = "SELECT n.*, nd.read, nt.notif_token_id,nm.notif_message_id
                    FROM notification n
                    INNER JOIN notif_message nm ON nm.reference_id = n.notification_id AND nm.reference_table = 'notification'
                    INNER JOIN notif_message_detail nd ON nd.notif_message_id = nm.notif_message_id
                    INNER JOIN notif_token nt ON nt.notif_token_id = nd.notif_token_id 
                    WHERE " . $where . " n.status > 0
                    ORDER BY created DESC
                    ";
                $r = db::get_result($q);
                foreach ($r as $key => $val) {
                    $read = element('read', $val);
                    $notif_message_id = element('notif_message_id', $val);
                    $notif_token_id = element('notif_token_id', $val);
                    if ($read == '0') {
                        $data_notif_mesage_detail = array(
                            'read' => '1'
                        );
                        $data_where = array(
                            'notif_message_id' => $notif_message_id,
                        );
                        $data_where['notif_token_id'] = $notif_token_id;
                        $ci->db->update('notif_message_detail', $data_notif_mesage_detail, $data_where);
                    }
                }

                $return['list'] = $data;
                if ($err_code > 0) {
                    $this->error()->add("NOTIFICATIONLIST[01]", $err_message);
                }

                return $return;
            }
        }

    }
    