<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Data_ReceivableList extends Api_Data_UserLogged {

        const __SORT_TRANSACTION_ASC = 'sort_transaction_asc';
        const __SORT_TRANSACTION_DESC = 'sort_transaction_desc';
        const __SORT_DUEDATE_ASC = 'sort_duedate_asc';
        const __SORT_DUEDATE_DESC = 'sort_duedate_desc';
        const __SORT_AMOUNT_ASC = 'sort_amount_asc';
        const __SORT_AMOUNT_DESC = 'sort_amount_desc';

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');
            $member_id = $this->session->get('member_id');

            $coa_code = element('coa_code', $post, '');
            $filter_user = element('filter_user', $post);
            $filter_price = element('filter_price', $post);
            $filter_status = element('filter_status', $post);
            $transaction_start = element('transaction_start', $post);
            $transaction_end = element('transaction_end', $post);
            $due_date_start = element('due_date_start', $post);
            $due_date_end = element('due_date_end', $post);
            $sort_by = element('sort', $post);
            $total_amount = 0;

            $err_code = 0;
            $err_message = '';
            $data = array();

            $order_by = '';

            if ($sort_by == self::__SORT_TRANSACTION_ASC) {
                $order_by = 'receivable_date ASC';
            }
            else if ($sort_by == self::__SORT_TRANSACTION_DESC) {
                $order_by = 'receivable_date DESC';
            }
            else if ($sort_by == self::__SORT_DUEDATE_ASC) {
                $order_by = 'receivable_due_date ASC';
            }
            else if ($sort_by == self::__SORT_DUEDATE_DESC) {
                $order_by = 'receivable_due_date DESC';
            }
            else if ($sort_by == self::__SORT_AMOUNT_ASC) {
                $order_by = 'amount ASC';
            }
            else if ($sort_by == self::__SORT_AMOUNT_DESC) {
                $order_by = 'amount DESC';
            }
            else {
                $order_by = 'receivable_date ASC';
            }

            $arr_where = array();

            if (strlen($filter_status) > 0) {
                if ($filter_status == "paid_off") {
                    $arr_where[] = "paid_off_status = 1";
                }
                else {
                    $arr_where[] = "paid_off_status = 0";
                }
            }

            if (strlen($transaction_start) > 0) {
                $arr_where[] = "DATE(receivable_date) >= " . db::escape(date("Y-m-d", strtotime($transaction_start)));
            }

            if (strlen($transaction_end) > 0) {
                $arr_where[] = "DATE(receivable_date) <= " . db::escape(date("Y-m-d", strtotime($transaction_end) + (60 * 60 * 24 * 1)));
            }

            if (strlen($due_date_start) > 0) {
                $arr_where[] = "DATE(receivable_due_date) >= " . db::escape(date("Y-m-d", strtotime($due_date_start)));
            }

            if (strlen($due_date_end) > 0) {
                $arr_where[] = "DATE(receivable_due_date) <= " . db::escape(date("Y-m-d", strtotime($due_date_end) + (60 * 60 * 24 * 1)));
            }

            if ($member_id != null) {
                $arr_where[] = "coa_code = " . db::escape($coa_code);
            }

            if (strlen($filter_user) > 0) {
                $arr_where[] = "description LIKE '%" . $ci->db->escape_like_str($filter_user) . "%'";
            }
            $arr_where_price = array();
            if (strlen($filter_price) > 0) {
                if (strpos($filter_price, '-')) {
                    $price = explode("-", $filter_price);
                    $arr_where_price[] = " amount >= " . db::escape($price[0]) . " ";
                    $arr_where_price[] = " amount <= " . db::escape($price[1]) . " ";
                }
                else {
                    $arr_where_price[] = " amount " . $ci->db->escape_like_str($filter_price) . " ";
                }
            }
            if (count($arr_where_price) > 0) {
                $arr_where[] = ' ( ' . implode(' AND ', $arr_where_price) . ' ) ';
            }

            $where = '';
            if (count($arr_where) > 0) {
                $where = implode(' AND ', $arr_where) . " AND ";
            }
            $count_data = 0;
            if ($err_code == 0) {
                $q = "SELECT * FROM receivable 
                      WHERE " . $where . "  status > 0
                      ORDER BY " . $order_by . "
                    ";
                $r = db::get_result($q);
                $count_data = count($r);
                foreach ($r as $key => $val) {
                    $data[] = array(
                        'receivable_id' => $val['receivable_id'],
                        'org_id' => $val['org_id'],
                        'coa_code' => $val['coa_code'],
                        'receivable_date' => date('d-m-Y', strtotime($val['receivable_date'])),
                        'receivable_due_date' => date('d-m-Y', strtotime($val['receivable_due_date'])),
                        'amount' => 'Rp.' . Helpers_Number::format_ind($val['amount']),
                        'ref_code' => $val['ref_code'],
                        'description' => $val['description'],
                        'paid_off_code' => $val['paid_off_code'],
                        'paid_off_date' => date('d-m-Y', strtotime($val['paid_off_date'])),
                        'paid_off_status' => $val['paid_off_status'],
                        'ref_id_import' => $val['ref_id_import'],
                        'created' => date('d-m-Y', strtotime($val['created'])),
                        'updated' => date('d-m-Y', strtotime($val['updated'])),
                    );
                    $total_amount = $total_amount + $val['amount'];
                };
                $return['list'] = $data;
                $return['total_data'] = $count_data;
                $return['total_amount'] = 'Rp.' . Helpers_Number::format_ind($total_amount);
                $return['query'] = $q;
            }

            if ($err_code > 0) {
                $this->error()->add("RECEIVABLELIST[01]", $err_message);
            }

            return $return;
        }

    }
    