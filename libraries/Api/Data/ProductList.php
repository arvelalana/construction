<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Data_ProductList extends Api_Data_UserLogged {

        const __SORT_NAME_ASC = 'sort_name_asc';
        const __SORT_NAME_DESC = 'sort_name_desc';
        const __SORT_PRICE_ASC = 'sort_price_asc';
        const __SORT_PRICE_DESC = 'sort_price_desc';
        const __SORT_STOCK_ASC = 'sort_stock_asc';
        const __SORT_STOCK_DESC = 'sort_stock_desc';

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');

            $filter_name = element('filter_name', $post);
            $filter_category = element('filter_category', $post);
            $filter_price = element('filter_price', $post);
            
            $sort_by = element('sort', $post);
            $filter = '';
            $order_by = '';

            if ($sort_by == self::__SORT_NAME_ASC) {
                $order_by = 'p.product_name ASC';
            }
            else if ($sort_by == self::__SORT_NAME_DESC) {
                $order_by = 'p.product_name DESC';
            }
            else if ($sort_by == self::__SORT_PRICE_ASC) {
                $order_by = 'p.sell_price ASC';
            }
            else if ($sort_by == self::__SORT_PRICE_DESC) {
                $order_by = 'p.sell_price DESC';
            }
            else if ($sort_by == self::__SORT_STOCK_ASC) {
                $order_by = 'p.qty ASC';
            }
            else if ($sort_by == self::__SORT_STOCK_DESC) {
                $order_by = 'p.qty DESC';
            }
            else {
                $order_by = 'p.product_name ASC';
            }

            $arr_where = array();

            if (strlen($filter_name) > 0) {
                $arr_where[] = "p.product_name LIKE '%" . $ci->db->escape_like_str($filter_name) . "%'";
//                $filter .= "p.product_name LIKE '%" . $filter_name . "%' AND ";
            }

            $arr_where_category = array();
            if (count($filter_category) > 0) {
                foreach ($filter_category as $key => $val) {
                    $arr_where_category[] = " p.product_category_code = " . db::escape($val) . " ";
//                    if (count($filter_category) - 1 == $key) {
//                        $filter .= "p.product_category_id = " . db::escape($val) . ") AND ";
//                    }
//                    else {
//                        $filter .= "p.product_category_id = " . db::escape($val) . " OR ";
//                    }
                }
            }

            if (count($arr_where_category) > 0) {
                $arr_where[] = ' ( ' . implode(' OR ', $arr_where_category) . ' ) ';
            }


            $arr_where_price = array();
            if (count($filter_price) > 0) {
                if (strpos($filter_price, '-')) {
                    $price = explode("-", $filter_price);
                    $arr_where_price[] = " p.sell_price >= " . db::escape($price[0]) . " ";
                    $arr_where_price[] = " p.sell_price <= " . db::escape($price[1]) . " ";
                }
                else {
                    $arr_where_price[] = " p.sell_price " . $ci->db->escape_like_str($filter_price) . " ";
                }
            }
            if (count($arr_where_price) > 0) {
                $arr_where[] = ' ( ' . implode(' AND ', $arr_where_price) . ' ) ';
            }
            
            $where = '';
            if (count($arr_where) > 0) {
                $where = implode(' AND ', $arr_where) . " AND ";
            }

            $err_code = 0;
            $err_message = '';
            $data = array();

            if ($err_code == 0) {
                $q = "SELECT p.*, pc.product_category_code, pc.product_category_name
                    FROM product p
                    INNER JOIN product_category pc ON p.product_category_code = pc.product_category_code
                    WHERE " . $where . " p.status > 0
                    ORDER BY " . $order_by . "
                    ";
                $r = db::get_result($q);

                foreach ($r as $k => $v) {
                    $data[] = array(
                        'product_id' => $v['product_id'],
                        'product_category_code' => $v['product_category_code'],
                        'product_category_name' => $v['product_category_name'],
                        'product_code' => $v['product_code'],
                        'product_name' => $v['product_name'],
                        'merk' => $v['merk'],
                        'sell_price' => number_format($v['sell_price']),
                        'qty' => $v['qty'],
                    );
                }
                $return['list'] = $data;
            }

            if ($err_code > 0) {
                $this->error()->add("USERLIST[01]", $err_message);
            }
            $return['query'] = $q;
            return $return;
        }

    }
    