<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Data_PoList extends Api_Data_UserLogged {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');
            $member_id = $this->session->get('member_id');


            $err_code = 0;
            $err_message = '';
            $data = array();

            if ($err_code == 0) {
                $where = '';

                $arr_where = array();
                if (strlen($member_id) > 0) {
                    $arr_where[] = " p.member_id = " . db::escape($member_id) . " ";
                }
                if (count($arr_where) > 0) {
                    $where = implode(' AND ', $arr_where) . ' AND ';
                }


                $q = "SELECT m.first_name,p.*
                    FROM purchase_order p
                    INNER JOIN member m ON p.member_id = m.member_id
                    WHERE " . $where . " p.status > 0
                    ORDER BY p.created DESC
                    ";
                $r = db::get_result($q);
                foreach ($r as $k => $v) {
                    $data[] = array(
                        'purchase_order_id' => $v['purchase_order_id'],
                        'from' => $v['first_name'],
                        'date' => date("d F Y", strtotime($v['created'])),
                        'time' => date("H:i", strtotime($v['created'])),
                        'message' => $v['message'],
                    );
                }
                $return['list'] = $data;
            }

            if ($err_code > 0) {
                $this->error()->add("POLIST[01]", $err_message);
            }

            return $return;
        }

    }
    