<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Data_UserDelete extends Api_Data_UserLogged {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');

            $err_code = 0;
            $err_message = '';

            $user_id = element('user_id', $post);

            if ($err_code == 0) {
                $data_member = array(
                    'updated' => date("Y-m-d H:i:s"),
                    'updatedby' => $username,
                    'status' => 0,
                );
                
                $q = "
                    SELECT member_id
                    FROM users 
                    WHERE user_id = " . db::escape($user_id) ." AND status > 0";
                $member_id = db::get_value($q);

                if ($member_id != null) {
                    $ci->db->update('member', $data_member, array('member_id' => $member_id));
                }

                $data_user = array(
                    'updated' => date("Y-m-d H:i:s"),
                    'updatedby' => $username,
                    'status' => 0,
                );
                $ci->db->update('users', $data_user, array('user_id' => $user_id));
                
            }

            if ($err_code == 0) {
                $return['message'] = lang('Member berhasil dihapus');
            }
            else {
                $this->error()->add("USREDT[01]", $err_message);
            }

            return $return;
        }

    }
    