<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Dec 14, 2017
     */
    class Api_Data_NotificationCreate extends Api_Data_UserLogged {

        protected $data_engine;

        public function __construct(Api_DataEngine $data_engine) {
            parent::__construct($data_engine);
            $this->data_engine = $data_engine;
        }

        public function exec($post) {
            $ci = & get_instance();
            $return = array();

            $username = $this->session->get('username');
            $user_id = $this->session->get('user_id');
            $org_id = $this->session->get('org_id');
            $member_id = $this->session->get('member_id');

            $err_code = 0;
            $err_message = '';

            $message = element('message', $post);
            $date = date("d-m-Y");
            if ($err_code == 0) {
                if (strlen($message) == 0) {
                    $err_code++;
                    $err_message = lang('Pesan harus diisi');
                }
            }

            
            if ($err_code == 0) {
                $data_po = array(
                    'org_id' => $org_id,
                    'member_id' => $member_id,
                    'message' => $message,
                    'created' => date("Y-m-d H:i:s"),
                    'createdby' => $username,
                    'updated' => date("Y-m-d H:i:s"),
                    'updatedby' => $username,
                    'status' => 1,
                );
                $ci->db->insert('notification', $data_po);
                $insert_id = $ci->db->insert_id();
            }

            if ($err_code == 0) {
                try {
                    Plugin_FCMNotification::factory('newNotification', $insert_id)->send();
                }
                catch (Exception $exc) {
                    $err_code++;
                    $err_message = $exc->getMessage();
                }
            }


            if ($err_code > 0) {
                $this->error()->add("NOTIFICATIONCREATE[01]", $err_message);
            }

            return $return;
        }

    }
    