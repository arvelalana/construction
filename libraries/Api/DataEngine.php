<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Oct 30, 2016
     */
    class Api_DataEngine extends Api_Engine {

        public function __construct() {
            parent::__construct();
        }

        public static function instance() {
            if (self::$instance == null) {
                self::$instance = new Api_DataEngine();
            }
            return self::$instance;
        }

        public function exec($post) {
            $ci = & get_instance();
            $session_id = element('session_id', $post);
            if (strlen($session_id) == 0) {
                $this->error()->add("SESS001", "Session ID is required");
            }

            if ($this->error()->get_err_code() == "0") {
                try {
                    $session = Api_SessionConstruction::instance($session_id);
                    $this->session_id = $session_id;
                }
                catch (Exception $exc) {
                    $this->error()->add("SESS002", "Session is not found");
                }
            }

            if ($this->error()->get_err_code() == "0") {
                $engine_method_name = 'Api_Data_' . ucfirst($this->method);
                $engine_method = new $engine_method_name($this);
            }

            if ($this->error()->get_err_code() == "0") {
                $data_log_api = array(
                    'session_id' => $this->session_id,
                    'web_code' => 'construction',
                    'app_code' => 'construction',
                    'service_name' => $this->method,
                    'request' => json_encode($post),
                    'login_request_date' => date("Y-m-d H:i:s"),
                );
                if (strtolower($this->method) != 'notiftoken') {
                    $ci->db->insert('log_api', $data_log_api);
                    $this->log_api_id = $ci->db->insert_id();
                }

                $data = $engine_method->exec($post);
                $this->data = $data;

                if (strtolower($this->method) != 'notiftoken') {
                    $session->load();
                    $session->set('data', $data);
                }
            }
        }

    }
    