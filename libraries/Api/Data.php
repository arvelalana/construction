<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Oct 30, 2016
     */
    class Api_Data {

        protected $session_id;

        /**
         *
         * @var Api_Session 
         */
        protected $session;

        public function __construct(Api_DataEngine $data_engine) {
            $session_id = $data_engine->get_session_id();
            $session = Api_SessionConstruction::instance($session_id);


            /**
             * @var Api_Session Description
             */
            $this->session = $session;
            $this->session_id = $session_id;
        }

        /**
         * 
         * @return Api_Error
         */
        protected function error() {
            return Api_Error::instance();
        }

        protected function dashboard() {
            // deadline minggu ini
            // total tugas yang belum selesai
            // total project 
            $user_id = $this->session->get('user_id');
            $sunday = date('Y-m-d', strtotime('sunday this week'));
            $sunday = date('Y-m-d', strtotime($sunday) - (60 * 60 * 24 * 7));
            $saturday = date('Y-m-d', strtotime('saturday this week'));
            $data_project = array();

            $q_deadline = "
                SELECT count(DISTINCT(t.to_do_list_id))
                FROM to_do_list t
                INNER JOIN to_do_list_employee te ON t.to_do_list_id = te.to_do_list_id AND te.status > 0
                INNER JOIN project p ON p.project_id = t.project_id
                WHERE (te.users_id = " . db::escape($user_id) . " OR t.users_id = " . db::escape($user_id) . ")
                    AND t.org_id = " . db::escape($this->session->get('org_id')) . "
                    AND p.status_project = 'ACTIVE'
                    AND (t.to_do_list_status = 'PENDING' OR t.to_do_list_status = 'ACTIVE' OR t.to_do_list_status = 'IN_PROGRESS') AND t.status > 0
                ";

            $q_deadline_this_week = $q_deadline . "
                AND DATE(t.plan_end_date) >= " . db::escape($sunday) . " AND DATE(t.plan_end_date) <= " . db::escape($saturday) . " ";
            $r_deadline_this_week = db::get_value($q_deadline_this_week);
            $return['deadline_this_week'] = $r_deadline_this_week;

            $r_deadline = db::get_value($q_deadline);
            $return['my_task_pending'] = $r_deadline;

            $q_my_project = "
                SELECT count(DISTINCT(p.project_id))
                FROM project p
                INNER JOIN project_employee pe ON p.project_id = pe.project_id
                WHERE (pe.users_id =  " . db::escape($user_id) . " OR p.users_id = " . db::escape($user_id) . ")
                    AND p.status_project = 'ACTIVE' 
                    AND p.org_id = " . db::escape($this->session->get('org_id')) . "
                    AND p.status > 0 AND pe.status > 0
                ";


            $r_my_project = db::get_value($q_my_project);
            $return['my_project'] = $r_my_project;
//
//            $q_project_date = "SELECT  pe.users_id as user_id_employee,u.username as username_employee, u1.username as username_creator, u.*, pe.status as pe_status, p.*
//                    FROM project p
//                    INNER JOIN project_employee pe on p.project_id = pe.project_id 
//                    INNER JOIN users u on pe.users_id = u.user_id 
//                    INNER JOIN users u1 on p.users_id = u1.user_id 
//                    WHERE  (p.users_id = " . db::escape($user_id) . " OR p.project_id IN (
//                            SELECT project_id
//                            FROM project_employee pe 
//                            WHERE pe.users_id = " . db::escape($user_id) . " AND status > 0    
//                    )) AND p.org_id = " . db::escape($this->session->get('org_id')) . " AND p.status > 0
//                    AND p.status_project = 'ACTIVE' 
//                    ORDER BY project_id DESC    
//                    ";
//
//            $r_project_date = db::get_result($q_project_date);
//            foreach ($r_project_date as $k => $v) {
//                $data_project[] = array(
//                    'project_id' => $v['project_id'],
//                    'org_id' => $v['org_id'],
//                    'users_id' => $v['users_id'],
//                    'project_code' => $v['project_code'],
//                    'project_name' => $v['project_name'],
//                    'description' => $v['description'],
//                    'status_project' => $v['status_project'],
//                    'plan_start_date' => date("Y-m-d", strtotime($v['plan_start_date'])),
//                    'plan_end_date' => date("Y-m-d", strtotime($v['plan_end_date'])),
//                    'actual_start_date' => date("Y-m-d", strtotime($v['actual_start_date'])),
//                    'actual_end_date' => date("Y-m-d", strtotime($v['actual_end_date'])),
//                    'created' => date("Y-m-d", strtotime($v['created'])),
//                    'createdby' => $v['createdby'],
//                    'updated' => date("Y-m-d", strtotime($v['updated'])),
//                    'updatedby' => $v['updatedby']
//                );
//            }

            $q = "SELECT  pe.users_id as user_id_employee,u.username as username_employee, u1.username as username_creator, u.*, pe.status as pe_status, p.*
                    FROM project p
                    INNER JOIN project_employee pe on p.project_id = pe.project_id 
                    INNER JOIN users u on pe.users_id = u.user_id 
                    INNER JOIN users u1 on p.users_id = u1.user_id 
                    WHERE  (p.users_id = " . db::escape($user_id) . " OR p.project_id IN (
                            SELECT project_id
                            FROM project_employee pe 
                            WHERE pe.users_id = " . db::escape($user_id) . " AND status > 0    
                    )) AND p.org_id = " . db::escape($this->session->get('org_id')) . " AND p.status > 0 
                    AND p.status_project = 'ACTIVE'
                    ORDER BY project_id DESC    
                    ";
            $r = db::get_result($q);
            foreach ($r as $k => $v) {
                $users = array();
                $temp_index = false;
                foreach ($data_project as $key => $val) {
                    if ($val['project_id'] == $v['project_id']) {
                        $users = $val['users'];
                        $temp_index = $key;
                    }
                }
                if ($temp_index !== false) {
                    $arr_data = $data_project[$temp_index];
                }
                else {
                    $arr_data = array(
                        'project_id' => $v['project_id'],
                        'user_id' => $v['users_id'],
                        'creator' => $v['username_creator'],
                        'project_code' => $v['project_code'],
                        'project_name' => $v['project_name'],
                        'description' => $v['description'],
                        'status_project' => $v['status_project'],
                        'plan_start_date' => date("Y-m-d", strtotime($v['plan_start_date'])),
                        'plan_end_date' => date("Y-m-d", strtotime($v['plan_end_date'])),
                        'actual_start_date' => date("Y-m-d", strtotime($v['actual_start_date'])),
                        'actual_end_date' => date("Y-m-d", strtotime($v['actual_end_date'])),
                        'created' => date("d M y", strtotime($v['created'])),
                        'createdby' => $v['createdby'],
                    );
                }

                if ($v['pe_status'] > 0) {
                    $users[] = array(
                        'user_id_employee' => $v['user_id_employee'],
                        'username_employee' => $v['username_employee'],
                        'first_name' => $v['first_name'],
                        'last_name' => $v['last_name'],
                        'email' => $v['email'],
                        'phone1' => $v['phone1'],
                    );
                }

                $arr_data['users'] = $users;
                if ($temp_index !== false) {
                    $data_project[$temp_index] = $arr_data;
                }
                else {
                    $data_project[] = $arr_data;
                }
            }

            $return['all_project'] = $data_project;
            return $return;
        }

        public function get_role_permission() {
            $role_id = $this->session->get('role_id');
            $arr_permissions = array();

            $q = "SELECT * 
                FROM role_permission 
                WHERE role_id = " . db::escape($role_id) . " AND status > 0";
            $r = db::get_result($q);
            foreach ($r as $k => $v) {
                $arr_permissions[$v['nav']] = array(
                    'name' => $v['name']
                );
            }
            $return['permissions'] = $arr_permissions;
            return $return;
        }

    }
    