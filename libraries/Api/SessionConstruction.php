<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Oct 20, 2016
     */
    class Api_SessionConstruction extends Api_Session {

        public static $instance = null;
        protected $session_id;
        protected $data;

        protected function __construct($session_id = null, $org_id = null) {

            $this->data = array();
            if ($session_id == null) {
                // create session
                // generate session_id
                $session_id = $org_id .'_' .date("YmdHis") . '_' . rand(100000, 999999);
                $this->session_id = $session_id;
            }
            else {
                // retrieve session
                $this->session_id = $session_id;
                $this->load();
            }
        }

        public static function instance($session_id = null, $org_id = null) {
            if (self::$instance == null) {
                self::$instance = new Api_SessionConstruction($session_id, $org_id);
            }
            return self::$instance;
        }
        
        public function save() {
            $path = APPPATH . 'logs';
            @mkdir($path);
            
            $temp = explode('_', $this->session_id);
            $org_id = element(0, $temp);
            $full_date = element(1, $temp);
            $random = element(2, $temp);
            
            $path .= '/' . $org_id;
            @mkdir($path);
            
            $year = substr($full_date, 0, 4);
            $path .= '/' . $year;
            @mkdir($path);

            $month = substr($full_date, 4, 2);
            $path .= '/' . $month;
            @mkdir($path);

            $date = substr($full_date, 6, 2);
            $path .= '/' . $date;
            @mkdir($path);

            $path .= '/' . $this->session_id . '.log';
            $string_data = serialize($this->data);
            file_put_contents($path, $string_data, LOCK_EX);
        }

        public function load() {
            $path = APPPATH . 'logs';
            
            $temp = explode('_', $this->session_id);
            $org_id = element(0, $temp);
            $full_date = element(1, $temp);
            $random = element(2, $temp);
            
            $path .= '/' . $org_id;
            
            $year = substr($full_date, 0, 4);
            $path .= '/' . $year;
            $month = substr($full_date, 4, 2);
            $path .= '/' . $month;
            $date = substr($full_date, 6, 2);
            $path .= '/' . $date;
            $path .= '/' . $this->session_id . '.log';

            if (file_exists($path)) {
                $string_data = file_get_contents($path);
                $this->data = unserialize($string_data);
            }
            else {
                throw new Exception("Session not found");
            }
        }

    }
    