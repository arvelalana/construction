<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Oct 30, 2016
     */
    class Api_Import {

        protected $session_id;

        /**
         *
         * @var Api_Session 
         */
        protected $session;

        public function __construct(Api_ImportEngine $import_engine) {
            $session_id = $import_engine->get_session_id();
            $session = Api_SessionConstruction::instance($session_id);


            /**
             * @var Api_Session Description
             */
            $this->session = $session;
            $this->session_id = $session_id;
        }

        /**
         * 
         * @return Api_Error
         */
        protected function error() {
            return Api_Error::instance();
        }


    }
    