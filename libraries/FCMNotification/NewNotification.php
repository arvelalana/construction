<?php

    /**
     *
     * @author Arvel Alana
     * @since  Sep 18, 2017
     */
    class FCMNotification_NewNotification extends Plugin_FCMNotification_Module {

        //put your code here
        public function __construct(Plugin_FCMNotification $fcm_notification) {
            parent::__construct($fcm_notification);
        }

        public function exec() {
            $ci = & get_instance();
            $data = array();


            $q = "
                SELECT n.*, o.web_code, o.app_code, o.org_code, o.org_id
                FROM notification n
                INNER JOIN org o ON n.org_id = o.org_id
                WHERE n.notification_id = " . $ci->db->escape($this->id) . "
                AND n.status > 0
                 ";
            $r = Helpers_DB::get_row($q);
            if (isset($r)) {
                $message = element('message', $r);
                $createdby = element('createdby', $r);
                $updatedby = element('updatedby', $r);
                $web_code = element('web_code', $r);
                $app_code = element('app_code', $r);
                $org_code = element('org_code', $r);
                $org_id = element('org_id', $r);
                $notification_id = element('notification_id', $r);

                $data['notif_message'] = array(
                    'web_code' => $web_code,
                    'app_code' => $app_code,
                    'org_code' => $org_code,
                    'module' => 'notification',
//                    'module_id' => $notification_id,
                    'title' => 'Pesan Baru',
                    'message' => $message,
                    'image' => 'null',
//                    'org_type' => $org_type,
                    'reference_id' => $this->id,
                    'reference_table' => 'notification',
                    'options' => array("module_id" => $this->id),
                    'created' => date("Y-m-d H:i:s"),
                    'createdby' => $createdby,
                    'updated' => date("Y-m-d H:i:s"),
                    'updatedby' => $updatedby,
                );

                $data_target = fcm_notification::all_user_org($org_id);
                $data['send_to'] = $data_target;
            }
            return $data;
        }

    }
    