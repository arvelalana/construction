<?php

    /**
     *
     * @author Arvel Alana
     * @since  Sep 18, 2017
     */
    class FCMNotification_NewPurchaseOrder extends Plugin_FCMNotification_Module {

        //put your code here
        public function __construct(Plugin_FCMNotification $fcm_notification) {
            parent::__construct($fcm_notification);
        }

        public function exec() {
            $ci = & get_instance();
            $data = array();


            $q = "
                SELECT m.first_name,po.*, o.web_code, o.app_code, o.org_code, o.org_id
                FROM purchase_order po
                INNER JOIN org o ON po.org_id = o.org_id
                INNER JOIN member m ON po.member_id = m.member_id
                WHERE po.purchase_order_id = " . $ci->db->escape($this->id) . "
                AND po.status > 0
                 ";
            $r = Helpers_DB::get_row($q);
            if (isset($r)) {
                $message = element('message', $r);
                $createdby = element('createdby', $r);
                $updatedby = element('updatedby', $r);
                $web_code = element('web_code', $r);
                $app_code = element('app_code', $r);
                $org_code = element('org_code', $r);
                $org_id = element('org_id', $r);
                $purchase_order_id = element('purchase_order_id', $r);
                $name = element('first_name', $r);


                $data['notif_message'] = array(
                    'web_code' => $web_code,
                    'app_code' => $app_code,
                    'org_code' => $org_code,
                    'module' => 'purchase_order',
//                    'module_id' => $purchase_order_id,
                    'title' => 'Pesanan Baru dari ' . $name,
                    'message' => $message,
                    'image' => 'null',
//                    'org_type' => $org_type,
                    'reference_id' => $this->id,
                    'reference_table' => 'purchase_order',
                    'options' => array("module_id" => $this->id),
                    'created' => date("Y-m-d H:i:s"),
                    'createdby' => $createdby,
                    'updated' => date("Y-m-d H:i:s"),
                    'updatedby' => $updatedby,
                );

                $data_target = fcm_notification::admin($org_id);
                $data['send_to'] = $data_target;
            }
            return $data;
        }

    }
    