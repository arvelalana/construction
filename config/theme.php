<?php

    return [
        'plugins' => [
            'jquery',
            'bootstrap-3.3.5',
            'font-awesome',
            'iCheck',
            'datatable',
            'select2',
            'bootbox',
            'bootstrap-slider',
            'wapp',
            'iproms',
        ]
    ];
    