<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  May 14, 2017
     */
    class fcm_notification {

        public static function all_user_org($org_id) {
            $ci = & get_instance();
            $q = "
                SELECT nt.* 
                FROM notif_token nt
                WHERE nt.org_id = " . $ci->db->escape($org_id) . " AND nt.status_token = 'ACTIVE'
                    AND nt.status > 0
                ";
            $r = db::get_result($q);
            $list_token = array();
            foreach ($r as $k => $v) {
                $list_token[] = array(
                    'notif_token_id' => element('notif_token_id', $v),
                    'token' => element('token', $v),
                );
            }
            return $list_token;
        }

        public static function admin($org_id) {
            $ci = & get_instance();
            $q = "
                SELECT nt.* 
                FROM notif_token nt
                WHERE nt.org_id = " . $ci->db->escape($org_id) . " AND nt.status_token = 'ACTIVE' AND nt.username= 'superadmin'
                    AND nt.status > 0
                ";
            $r = db::get_result($q);
            $list_token = array();
            foreach ($r as $k => $v) {
                $list_token[] = array(
                    'notif_token_id' => element('notif_token_id', $v),
                    'token' => element('token', $v),
                );
            }
            return $list_token;
        }

    }
    