<?php

    /**
     *
     * @author Arvel Alana
     * @since  Aug 10, 2017
     */
    return [
        'select' => [
            'role' => [
                'class' => 'Input_Select_Role',
                'method' => 'ajax'
            ],
            'org' => [
                'class' => 'Input_Select_Org',
                'method' => 'ajax'
            ],
            'division' => [
                'class' => 'Input_Select_Division',
                'method' => 'ajax'
            ],
            'employee-position' => [
                'class' => 'Input_Select_EmployeePosition',
                'method' => 'ajax'
            ],
            'user' => [
                'class' => 'Input_Select_User',
                'method' => 'ajax'
            ],
            'project' => [
                'class' => 'Input_Select_Project',
                'method' => 'ajax'
            ],
            'meeting' => [
                'class' => 'Input_Select_Meeting',
                'method' => 'ajax'
            ],
        ]
    ];

    