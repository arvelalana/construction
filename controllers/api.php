<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Aug 10, 2017
     */
    class Controller_Api extends WController {
        
        public function __construct() {
            parent::__construct();
        }
        
        public function session(){
            $post = $_GET + $_POST;
            
            $engine = Api_SessionEngine::instance();
            $exec = $engine->exec($post);
            $response = $engine->response();
//            $request = array("test" => 'a');
            $request = $post;
            echo json_encode($response);
        }
        
        public function data($method){
            $post = $_GET + $_POST;
            
            $engine = Api_DataEngine::instance();
            $engine->set_method($method);
            $exec = $engine->exec($post);
            $response = $engine->response();
//            $request = array("test" => 'a');
            $request = $post;
            echo json_encode($response);
        }
        
        public function import($method){
            $post = $_FILES + $_GET + $_POST;
            
            $engine = Api_ImportEngine::instance();
            $engine->set_method($method);
            $exec = $engine->exec($post);
            $response = $engine->response();
//            $request = array("test" => 'a');
            $request = $post;
            echo json_encode($response);
        }
        
        public function testUpload($name){
            $path = APPPATH .'logs/test.log';
            $files = $_FILES;
            file_put_contents($path, json_encode($files));
//            echo "TESTUPLOAD";
            
            $wapp = WApp::instance();
            $wapp->set_data_type('json');
            $wapp->set_ajax(true);

//            $file = json_decode('{"file":{"name":"CS405-1.1-WATSON.pdf","type":"application\/pdf","tmp_name":"C:\\xampp\\tmp\\php3FC6.tmp","error":0,"size":1303555}}', true);
            
            $files = $_FILES;
            $file = $files[$name];
            $document_name = $file['name'];
            $document_tmp_path = $file['tmp_name'];
//            $document_file = file_get_contents($document_tmp_path);

            $err_code = 0;
            $err_message = '';
            $data = array();

            try {
                $resource = Resource::factory('document', $name);
                $resource->set_temporary(true);

                if ($err_code == 0) {
                    $filename = $resource->save($document_name, $file);
                    $secure_filename = $resource->get_filename(true);
                    $path_filename = $resource->get_path_filename();
                    $original_filename = $resource->get_original_filename();
                    $url_preview = base_url() . 'resource/tmp_document/' . urlencode($secure_filename);
                    $data = array(
//                        'ext'=>$file['type'],
                        'filename' => $secure_filename,
                        'uri_filename' => $original_filename,
                        'ori_filename' => $document_name,
                        'url_preview' => $url_preview,
                        'url' => base_url() . 'resource/tmp_document/' . urlencode($secure_filename),
                    );
                }
            }
            catch (Exception $exc) {
                $err_code++;
                $err_message = $exc->getMessage();
            }

            $status = 'SUCCESS';
            if ($err_code > 0) {
                $status = 'ERROR';
            }

            $output = array(
                'status' => $status,
                'message' => $err_message,
                'data' => $data,
            );
            echo json_encode($output);
        }
    }
    